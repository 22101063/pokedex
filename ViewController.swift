//
//  ViewController.swift
//  Aula_23062022
//
//  Created by COTEMIG on 23/06/22.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var listaDeValores = [String]()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeValores.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tabela.dequeueReusableCell(withIdentifier: "celula", for: indexPath) as? Celula
        
        cell?.titulo.text = listaDeValores[indexPath.row]
        
        
        if indexPath.row % 3 == 0 {
            cell?.imagem.image = UIImage(systemName: "house")
            cell?.container.backgroundColor = .yellow
        } else {
            cell?.container.backgroundColor = .green
        }
        
        return cell ?? UITableViewCell()
    }
    

    @IBOutlet weak var tabela: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        listaDeValores.append("Bulbassauro")
        listaDeValores.append("Ivyssauro")
        listaDeValores.append("Venossauro")
        listaDeValores.append("Charmander")
        
        tabela.dataSource = self
        tabela.delegate = self
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "detalhes", sender: nil)
    }
}

